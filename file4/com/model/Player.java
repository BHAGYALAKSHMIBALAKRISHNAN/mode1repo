package com.model;

public class Player {
String Name;
protected int age;
public Player() {
	super();
}
public Player(String name, int age) {
	super();
	Name = name;
	this.age = age;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}

}
