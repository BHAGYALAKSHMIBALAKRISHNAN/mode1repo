package com.model;

public class SquareSum {
	public static int sumofsquarenumber(int number) {
		int n;
		int sum=0;
		while(number>0){
			n=number%10;
			if(n%2==0)
				sum+=sum+n*n;
			number/=10;
			
		}
		return sum;
	}

}
