package com.main;

import com.model.FirstClass5;
import com.model.SecondClass5;

public class OopExercises5 {

	private Object objB;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
FirstClass5 firstclass= new FirstClass5();
SecondClass5 secondclass= new SecondClass5();

System.out.println("in main():");
System.out.println("-----in the constructor of class firstclass :");
int obj1=firstclass.setA1(100);
int obj11=firstclass.setA1(300);
System.out.println(+obj1);
System.out.println(obj11);
System.out.println("-----in the constructor of class B :");

double obj2=secondclass.setB(123.45);
double obj22=secondclass.setB(314.159);
System.out.println(+obj2);
System.out.println(obj22);

	}

}
