package com.main;

import com.model.ClassA;
import com.model.ClassB;
import com.model.ClassC;

public class MainClass {
	public static void main(String[] args) {
		ClassA objA=new ClassA();
		ClassB objB1=new ClassB();
		ClassB objB2=new ClassB();
		ClassC objC1=new ClassC();
		ClassC objC2=new ClassC();
		ClassC objC3=new ClassC();
		objA.display();
		objB1.display();
		objB2.display();
		objC1.display();
		objC2.display();
		objC3.display();
	}

}
