package com.model;

public class Rectangle {
	Integer Length;
	Integer Breadth;
	
	public Rectangle() {
		super();
	}

	public Rectangle(Integer length, Integer breadth) {
		super();
		Length = length;
		Breadth = breadth;
	}

	public Integer getLength() {
		return Length;
	}

	public void setLength(Integer length) {
		Length = length;
	}

	public Integer getBreadth() {
		return Breadth;
	}

	public void setBreadth(Integer breadth) {
		Breadth = breadth;
	}
	
	public int calculateArea() {
		// TODO Auto-generated method stub
		return Length*Breadth;
	}
}
