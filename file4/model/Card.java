package com.model;

public abstract class Card {
	String holderName;
	String CardNumber;
	String expiryDate;
	public Card(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		CardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}
	public Card() {
		super();
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return CardNumber;
	}
	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

}
