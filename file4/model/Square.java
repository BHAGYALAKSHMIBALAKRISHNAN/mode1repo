package com.model;

public class Square extends Shape {
	Integer side;

	public Square() {
		super();
	}

	public Square(String name, Integer side) {
		super(name);
		this.side = side;
	}

	public Integer getSide() {
		return side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}

	public int calculateArea() {
		// TODO Auto-generated method stub
		return side*side;
	}
	

}
