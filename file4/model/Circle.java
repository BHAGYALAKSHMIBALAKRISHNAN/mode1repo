package com.model;

public class Circle extends Shape {
Integer radius;

public Circle() {
	super();
}

public Circle(String name, Integer radius) {
	super(name);
	this.radius = radius;
}

public Integer getRadius() {
	return radius;
}

public void setRadius(Integer radius) {
	this.radius = radius;
}
public float calculateArea() {
	// TODO Auto-generated method stub
	return (float) (2*3.14*radius);
}
}
