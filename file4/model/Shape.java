package com.model;

public abstract class Shape {
String Name;

public Shape() {
	super();
}

public Shape(String name) {
	super();
	Name = name;
}

public String getName() {
	return Name;
}

public void setName(String name) {
	Name = name;
}


}
