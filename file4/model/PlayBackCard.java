package com.model;

public class PlayBackCard  extends Card{
int pointsEarned;
Double totalAmount;

public int getPointsEarned() {
	return pointsEarned;
}
public void setPointsEarned(int pointsEarned) {
	this.pointsEarned = pointsEarned;
}
public Double getTotalAmount() {
	return totalAmount;
}
public void setTotalAmount(Double totalAmount) {
	this.totalAmount = totalAmount;
}
public PlayBackCard(String holderName, String cardNumber, String expiryDate) {
	super(holderName, cardNumber, expiryDate);
}
public PlayBackCard(String holderName, String cardNumber, String expiryDate, int pointsEarned, Double totalAmount) {
	super(holderName, cardNumber, expiryDate);
	this.pointsEarned = pointsEarned;
	this.totalAmount = totalAmount;
}

}
